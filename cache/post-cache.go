package cache

import "store-api/models"

type PostCache interface {
	Set(key string, value *models.Book)
	Get(key string) *models.Book
}
