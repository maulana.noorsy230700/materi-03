package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Book struct {
	ID    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Isbn  string             `json:"isbn" bson:"isbn,omitempty"`
	Title string             `json:"title" bson:"title,omitempty"`
	Store *Store             `json:"store" bson:"store,omitempty"`
}

type Store struct {
	Name    string `json:"name,omitempty" bson:"name,omitempty"`
	Address string `json:"address" bson:"address,omitempty"`
}
