package condb

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//MongoDB Connection
func ConnectMongoDB() *mongo.Collection {
	//client options
	clientOptions := options.Client().ApplyURI("mongodb+srv://maulananoorsy:maulananoorsy@cluster0.57m9j6y.mongodb.net/?retryWrites=true&w=majority")

	//Connecting mongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connection Success")

	collection := client.Database("belajarMongoDB").Collection("books")

	return collection

}
