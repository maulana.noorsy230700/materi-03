package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	condb "store-api/ConDB"
	"store-api/cache"
	"store-api/models"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	collection                 = condb.ConnectMongoDB()
	postCache  cache.PostCache = cache.NewRedisCache("localhost:6379", 0, 10)
)

func main() {

	//Routing
	r := mux.NewRouter()

	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	r.HandleFunc("/api/books", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")

	//Starting Server
	log.Fatal(http.ListenAndServe(":8080", r))

}

func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var books []models.Book

	data, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		fmt.Println(err)
		return
	}

	defer data.Close(context.TODO())

	for data.Next(context.TODO()) {
		var book models.Book

		err := data.Decode(&book)
		if err != nil {
			log.Fatal(err)
		}

		books = append(books, book)
	}

	if err := data.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(books)

}

func getBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var book models.Book

	//getting param
	var param = mux.Vars(r)

	// convert string to primitive.ObjectID
	id, _ := primitive.ObjectIDFromHex(param["id"])

	filter := bson.M{"_id": id}

	postID := strings.Split(r.URL.Path, "/")[3]

	var post *models.Book = postCache.Get(postID)

	if post == nil {
		err := collection.FindOne(context.TODO(), filter).Decode(&book)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Println(err)
			return
		}

		json.NewEncoder(w).Encode(book)
		postCache.Set(postID, &book)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(book)
	}

	// fmt.Println(book)

	// err := collection.FindOne(context.TODO(), filter).Decode(&book)

	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }

	// json.NewEncoder(w).Encode(book)
}

func createBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var book models.Book

	_ = json.NewDecoder(r.Body).Decode(&book)

	result, err := collection.InsertOne(context.TODO(), book)

	if err != nil {
		fmt.Println(err)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func updateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var param = mux.Vars(r)

	//get id from param
	id, _ := primitive.ObjectIDFromHex(param["id"])

	var book models.Book

	// Filter
	filter := bson.M{"_id": id}

	_ = json.NewDecoder(r.Body).Decode(&book)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"isbn", book.Isbn},
			{"title", book.Title},
			{"store", bson.D{
				{"name", book.Store.Name},
				{"address", book.Store.Address},
			}},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&book)

	if err != nil {
		fmt.Println(err)
		return
	}

	book.ID = id

	json.NewEncoder(w).Encode(book)
}

func deleteBook(w http.ResponseWriter, r *http.Request) {
	// Set header
	w.Header().Set("Content-Type", "application/json")

	// get params
	var param = mux.Vars(r)

	// string to primitve.ObjectID
	id, err := primitive.ObjectIDFromHex(param["id"])

	// prepare filter.
	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		fmt.Println(err)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}
